'use strict';

angular.module('myApp.view1', ['ngRoute', 'ngSanitize'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/elevator', {
            templateUrl: 'view1/view1.html',
            controller: 'View1Ctrl',
            controllerAs: 'ViewCtrl'
        });
    }])

    .controller('View1Ctrl', ['$interval', function ($interval) {
        var me = this;
        me.ctrl = {
            pisos: [5, 4, 3, 2, 1],
            numUsers: 1,
            log: [],
            ascensores: {
                "1": {
                    ascensorId: 1,
                    status: "disponible",
                    text: "Ascensor #1",
                    usersInAscensor: 0,
                    marginTopOrigin: "500",
                    marginTopModify: "500",
                    transitionAscensor: "margin 7s",
                    pisoDirigido: null,
                    inProgress: false
                },
                "2": {
                    ascensorId: 2,
                    status: "disponible",
                    text: "Ascensor #2",
                    usersInAscensor: 0,
                    marginTopOrigin: "500",
                    marginTopModify: "500",
                    transitionAscensor: "margin 7s",
                    pisoDirigido: null,
                    inProgress: false
                },
                "3": {
                    ascensorId: 3,
                    status: "disponible",
                    text: "Ascensor #3",
                    usersInAscensor: 0,
                    marginTopOrigin: "500",
                    marginTopModify: "500",
                    transitionAscensor: "margin 7s",
                    pisoDirigido: null,
                    onprogress: false
                }
            },
            pisosTop: {
                5: {
                    userInProgress: 0,
                    ascensorDirigido: null,
                    ascensorArecoger: null,
                    margin: 0
                },
                4: {
                    userInProgress: 0,
                    ascensorDirigido: null,
                    ascensorArecoger: null,
                    margin: 125,
                },
                3: {
                    userInProgress: 0,
                    ascensorDirigido: null,
                    ascensorArecoger: null,
                    margin: 250
                },
                2: {
                    userInProgress: 0,
                    ascensorDirigido: null,
                    ascensorArecoger: null,
                    margin: 375
                },
                1: {
                    userInProgress: 0,
                    ascensorDirigido: null,
                    ascensorArecoger: null,
                    margin: 500
                },

            },
            addUserToPiso: function (piso) {
                me.ctrl.pisoAir = piso;
                console.log("user add in piso: " + piso);
            },
            executeAscensor: function () {
                me.ctrl.pisosTop[me.ctrl.pisoAir].userInProgress = me.ctrl.pisosTop[me.ctrl.pisoAir].userInProgress + me.ctrl.numUsers;
                me.ctrl.pisosTop[me.ctrl.pisoAir].ascensorDirigido = me.ctrl.pisoAir;
                me.ctrl.pisosTop[me.ctrl.pisoAir].ascensorArecoger = angular.copy(me.ctrl.currentPiso);

                //me.ctrl.ascensores["1"].marginTopModify = 125;
            },
            init: function () {
                $interval(function () {
                    console.log("checked");
                    angular.forEach(me.ctrl.pisosTop, function (piso) {
                        if (piso.userInProgress > 0 && piso.ascensorDirigido != null) {
                            var pisoSelected = false;
                            angular.forEach(me.ctrl.ascensores, function (ascensor) {
                                console.log(ascensor);
                                if (ascensor.status == 'disponible' && !pisoSelected) {
                                    me.ctrl.ascensores[ascensor.ascensorId].usersInAscensor = angular.copy(piso.userInProgress);
                                    me.ctrl.ascensores[ascensor.ascensorId].status = "procesando";
                                    me.ctrl.ascensores[ascensor.ascensorId].pisoDirigido = angular.copy(piso.ascensorDirigido);
                                    me.ctrl.ascensores[ascensor.ascensorId].pisoArecoger = angular.copy(piso.ascensorArecoger);
                                    me.ctrl.ascensores[ascensor.ascensorId].inProgress = true;
                                    me.ctrl.log.push({
                                        'piso': piso.ascensorArecoger,
                                        'date': new Date().getFullYear() + '/' + new Date().getMonth() + '/' + new Date().getDay(),
                                        'message': "Bienvenidos usuario(s) del piso <b>" + piso.ascensorArecoger + "</b>, por favor diriganse a la puerta del ascensor <b>" + ascensor.ascensorId + "</b>"
                                    });
                                    piso.userInProgress = 0;
                                    piso.ascensorDirigido = null;
                                    pisoSelected = true;
                                }
                            });
                        }
                    });
                }, 7000);

                $interval(function () {
                    angular.forEach(me.ctrl.ascensores, function (ascensor) {
                        if (ascensor.status == 'procesando' && ascensor.inProgress) {
                            ascensor.inProgress = false;
                            ascensor.marginTopModify = me.ctrl.pisosTop[ascensor.pisoArecoger].margin;
                            setTimeout(function () {
                                ascensor.marginTopModify = me.ctrl.pisosTop[angular.copy(ascensor.pisoDirigido)].margin;
                                ascensor.pisoDirigido = null;
                                ascensor.pisoArecoger = null;
                                setTimeout(function () {
                                    ascensor.status = "disponible";
                                    ascensor.usersInAscensor = 0;
                                }, 7000);
                            }, 14000);
                            console.log(ascensor);
                        }
                    });
                }, 7000);
            }
        };
        me.ctrl.init();


    }]);